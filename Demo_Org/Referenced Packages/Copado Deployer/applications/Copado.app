<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Getting_Started</defaultLandingTab>
    <label>Copado</label>
    <logo>Copado/Copado_Logo.png</logo>
    <tab>Getting_Started</tab>
    <tab>Org__c</tab>
    <tab>Deployment__c</tab>
    <tab>OrgDiff__c</tab>
    <tab>Account_Summary</tab>
    <tab>Git_Repository__c</tab>
    <tab>Git_Backup__c</tab>
    <tab>standard-report</tab>
    <tab>Environment__c</tab>
    <tab>Snapshot_Difference__c</tab>
    <tab>Continuous_Integration__c</tab>
    <tab>Copado_License_Manager</tab>
    <tab>Scheduled_Job__c</tab>
</CustomApplication>
