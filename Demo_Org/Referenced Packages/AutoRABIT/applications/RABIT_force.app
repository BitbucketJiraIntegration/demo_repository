<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>RABIT_force</defaultLandingTab>
    <description>AutoRABIT stands for Rapid Automated Build Install Test Framework which is designed to be a powerful continuous delivery system for force.com application development.</description>
    <label>AutoRABIT</label>
    <logo>rabit_img/AutoRABIT_Logo.png</logo>
    <tab>RABIT_force</tab>
</CustomApplication>
